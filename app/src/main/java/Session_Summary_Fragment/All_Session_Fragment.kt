package Session_Summary_Fragment

import Adapters.All_Session_Recycler_Adapter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pukaar_therapist.R
import com.example.pukaar_therapist.databinding.FragmentAllSessionBinding


class All_Session_Fragment : Fragment() {

    lateinit var allSessionBinding: FragmentAllSessionBinding

    var patient_name : List<String> = listOf("Aline Js" , "Maliha Ashfeen" )
    var date : List<String> = listOf("20/01/2021" , "20/01/2021" )
    var time : List<String> = listOf("03:09PM" , "03:09PM" )

    var session_taken : List<String> = listOf("02" , "08" )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        allSessionBinding = FragmentAllSessionBinding.inflate(inflater , container , false)

        val recyclerView = allSessionBinding.allSessionRecycler
        recyclerView.adapter = All_Session_Recycler_Adapter(patient_name , date , time , session_taken)
        recyclerView.layoutManager = LinearLayoutManager(context , LinearLayoutManager.VERTICAL , false)

        return allSessionBinding.root
    }


}