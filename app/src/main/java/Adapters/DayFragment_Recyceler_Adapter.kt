package Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pukaar_therapist.CommonFunction
import com.example.pukaar_therapist.R
import com.example.pukaar_therapist.Responces.*
import org.w3c.dom.Text

class DayFragment_Recyceler_Adapter(val context: Context , val unassigned_data: ArrayList<DataDiary1>) : RecyclerView.Adapter<DayFragment_viewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayFragment_viewHolder {
        val  inflater : LayoutInflater = LayoutInflater.from(parent.context)
        val view : View = inflater.inflate(R.layout.diary_recyler_view , parent , false)
        return DayFragment_viewHolder(view)
    }

    override fun onBindViewHolder(holder: DayFragment_viewHolder, position: Int) {
        /*holder.profile_image.setImageResource(profile_image[position])*/
        holder.title.text = unassigned_data[position].mood
        holder.time.text = CommonFunction.dateFormat(unassigned_data[position].created_at)
        holder.date.text = CommonFunction.dateFormat(unassigned_data[position].created_at)
    }

    override fun getItemCount(): Int {
        return unassigned_data.size
    }
}

class  DayFragment_viewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
    var  profile_image = itemView.findViewById<ImageView>(R.id.circleImageView)
    var title = itemView.findViewById<TextView>(R.id.titleTextView1)
    var time =itemView.findViewById<TextView>(R.id.timeTextView)
    var date =itemView.findViewById<TextView>(R.id.dateTextView)


}