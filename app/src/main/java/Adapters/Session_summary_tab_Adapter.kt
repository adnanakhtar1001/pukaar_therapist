package Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class Session_summary_tab_Adapter(fragment_manager : FragmentManager) : FragmentStatePagerAdapter(fragment_manager , BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    val fragments : ArrayList<Fragment> = ArrayList()
    val titles : ArrayList<String> = ArrayList()
    override fun getCount(): Int {
        return  fragments.size
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

    fun addfragments(fragment: Fragment , title : String){
      fragments.add(fragment)
      titles.add(title)
    }
}