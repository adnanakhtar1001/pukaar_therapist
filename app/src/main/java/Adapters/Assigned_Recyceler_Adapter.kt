package Adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.pukaar_therapist.CommonFunction
import com.example.pukaar_therapist.Fragments.Daily_Dairy_Fragment
import com.example.pukaar_therapist.Fragments.Day_Fragment
import com.example.pukaar_therapist.Fragments.Set_mood_Fragment
import com.example.pukaar_therapist.R

import com.example.pukaar_therapist.Responces.Users
import com.example.pukaar_therapist.Responces.UsersData
import org.w3c.dom.Text

class Assigned_Recyceler_Adapter(val context: Context , val unassigned_data: ArrayList<UsersData>) : RecyclerView.Adapter<Assigned_viewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Assigned_viewHolder {
        val  inflater : LayoutInflater = LayoutInflater.from(parent.context)
        val view : View = inflater.inflate(R.layout.assigned_recyler_design , parent , false)
        return Assigned_viewHolder(view)
    }

    override fun onBindViewHolder(holder: Assigned_viewHolder, position: Int) {
        /*holder.profile_image.setImageResource(profile_image[position])*/
        holder.profile_name.text = unassigned_data[position].first_name +  " " + unassigned_data[position].last_name
        holder.time.text = CommonFunction.dateFormat(unassigned_data[position].created_at)
        holder.user_mood.setOnClickListener{
            val transaction = (context as AppCompatActivity).supportFragmentManager.beginTransaction()
            /*var setMoodFragment : Set_mood_Fragment*/
            var fragment = Set_mood_Fragment()
             var bundle =  Bundle()
            bundle.putString("id",unassigned_data[position].id.toString());

            fragment.arguments = bundle
            transaction.replace(R.id.container, fragment)
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
        holder.user_dairy.setOnClickListener{
            val transaction = (context as AppCompatActivity).supportFragmentManager.beginTransaction()
            var fragment = Daily_Dairy_Fragment()
            var bundle =  Bundle()
            bundle.putString("id",unassigned_data[position].id.toString());
            fragment.arguments = bundle
            transaction.replace(R.id.container, fragment)
            transaction.disallowAddToBackStack()
            transaction.commit()
        }
    }

    override fun getItemCount(): Int {
        return unassigned_data.size
    }
}

class  Assigned_viewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
    var  user_mood = itemView.findViewById<ImageView>(R.id.user_mood)
    var user_dairy = itemView.findViewById<ImageView>(R.id.user_dairy)
    var profile_name = itemView.findViewById<TextView>(R.id.profile_name1)
    var time =itemView.findViewById<TextView>(R.id.unassigned_time1)

}