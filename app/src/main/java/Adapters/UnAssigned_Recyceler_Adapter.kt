package Adapters

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.pukaar_therapist.APIClient.APIClient
import com.example.pukaar_therapist.API_Interface.API_Interfaces
import com.example.pukaar_therapist.CommonFunction
import com.example.pukaar_therapist.R
import com.example.pukaar_therapist.Responces.Users
import com.example.pukaar_therapist.Responces.UsersData
import com.example.pukaar_therapist.Responces.UsersData1
import kotlinx.android.synthetic.main.take_client.*
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UnAssigned_Recyceler_Adapter(val context: Context , val unassigned_data: ArrayList<UsersData1>) : RecyclerView.Adapter<UnAssigned_viewHolder>() {
    lateinit var apiInterface : API_Interfaces
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UnAssigned_viewHolder {
        val  inflater : LayoutInflater = LayoutInflater.from(parent.context)
        val view : View = inflater.inflate(R.layout.unassigned_recyler_design , parent , false)
        return UnAssigned_viewHolder(view)
    }

    override fun onBindViewHolder(holder: UnAssigned_viewHolder, position: Int) {
        /*holder.profile_image.setImageResource(profile_image[position])*/
        holder.profile_name.text = unassigned_data[position].first_name +  " " + unassigned_data[position].last_name ;
        holder.time.text = CommonFunction.dateFormat(unassigned_data[position].created_at)
        holder.itemView.setOnClickListener {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.window?.setBackgroundDrawableResource(R.drawable.rounded_dialuge)
            dialog.setContentView(R.layout.take_client)
            dialog.client_namae.text =  unassigned_data[position].first_name +  " " + unassigned_data[position].last_name
          dialog.show()

            apiInterface = APIClient.create()


            dialog.reject_button.setOnClickListener {
                dialog.dismiss()
            }

            dialog.accept_button.setOnClickListener {
                var status : String = "approved"

                val call =
                    apiInterface.getUserSessionUpdate(CommonFunction.getToken(context),  unassigned_data[position].id , CommonFunction.get_id(context) )
                call.enqueue(object : Callback<String> {
                    override fun onResponse(
                        call: Call<String>?,
                        response: Response<String>?
                    ) {
                        if (response?.body() != null) {
                            if(response.body().equals( "Success"))
                                Toast.makeText(context , "Approved" , Toast.LENGTH_SHORT)
                            dialog.dismiss()
                        }
                    }
                    override fun onFailure(call: Call<String>?, t: Throwable?) {

                    }
                })
            }




        }
    }

    override fun getItemCount(): Int {
        return unassigned_data.size
    }
}

class  UnAssigned_viewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
    var  profile_image = itemView.findViewById<ImageView>(R.id.circleImageView)
    var profile_name = itemView.findViewById<TextView>(R.id.profile_name)
    var time =itemView.findViewById<TextView>(R.id.unassigned_time)


}