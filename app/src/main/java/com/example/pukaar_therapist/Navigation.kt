package com.example.pukaar_therapist

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toolbar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.pukaar_therapist.Fragments.*
import com.example.pukaar_therapist.databinding.ActivityNavigationBinding
import com.google.android.material.navigation.NavigationView

class Navigation : AppCompatActivity() , NavigationView.OnNavigationItemSelectedListener {

    lateinit var navigationBinding: ActivityNavigationBinding

    lateinit var dashBoard_Fragment: DashBoard_Fragment
    lateinit var notification_fragment: Notification_Fragment

    lateinit var sessionSummaryFragment: Session_summary_Fragment
    lateinit var notificationFragment: Notification_Fragment
    lateinit var settingFragment: setting_Fragment
    lateinit var pendingRequestsFragment: Pending_requests_Fragment
    lateinit var contactFragment: Contact_Fragment
    lateinit var aboutFragment: About_Fragment
    lateinit var patientProfileFragment: Patient_Profile_Fragment
    lateinit var textView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigationBinding = ActivityNavigationBinding.inflate(layoutInflater)
        setContentView(navigationBinding.root)



        navigationBinding.titleToolbar.setText("Adnan")

        val toolbar = navigationBinding.toolbar;
        setSupportActionBar(toolbar);
        toolbar.setTitle("")
        navigationBinding.dehaze.setOnClickListener {
            navigationBinding.drawerLayout.openDrawer(GravityCompat.START)
        }
navigationBinding.bottomNavigation1.setOnNavigationItemSelectedListener  { item ->
    when(item.itemId){
        R.id.nav_profile -> {
            patientProfileFragment = Patient_Profile_Fragment()
            supportFragmentManager.beginTransaction().replace(R.id.container , patientProfileFragment).commit()
        }
        R.id.nav_room -> {
            sessionSummaryFragment = Session_summary_Fragment()
            supportFragmentManager.beginTransaction().replace(R.id.container , sessionSummaryFragment).commit()
        }
        R.id.nav_home -> {
            dashBoard_Fragment = DashBoard_Fragment()
            supportFragmentManager.beginTransaction().replace(R.id.container , dashBoard_Fragment).commit()
        }
        R.id.bottom_settting -> {
            settingFragment = setting_Fragment()
            supportFragmentManager.beginTransaction().replace(R.id.container , settingFragment).commit()
        }


    }
    true
}

        navigationBinding.navView.setNavigationItemSelectedListener(this)

        dashBoard_Fragment = DashBoard_Fragment()
        supportFragmentManager.beginTransaction().replace(R.id.container, dashBoard_Fragment)
            .commit()

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.left_session_summary -> {

                sessionSummaryFragment = Session_summary_Fragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container, sessionSummaryFragment).commit()
            }

            R.id.left_notification -> {

                notificationFragment = Notification_Fragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container, notificationFragment).commit()
            }
            R.id.left_setting -> {

                settingFragment = setting_Fragment()
                supportFragmentManager.beginTransaction().replace(R.id.container, settingFragment)
                    .commit()
            }
            R.id.left_prnding_request -> {

                pendingRequestsFragment = Pending_requests_Fragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container, pendingRequestsFragment).commit()
            }

            R.id.left_contact_support -> {

                contactFragment = Contact_Fragment()
                supportFragmentManager.beginTransaction().replace(R.id.container, contactFragment)
                    .commit()
            }
            R.id.left_about -> {

                aboutFragment = About_Fragment()
                supportFragmentManager.beginTransaction().replace(R.id.container, aboutFragment)
                    .commit()
            }
            R.id.left_logout -> {

                val intent = Intent(applicationContext , Login::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
                finish()
            }




        }

        navigationBinding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

}