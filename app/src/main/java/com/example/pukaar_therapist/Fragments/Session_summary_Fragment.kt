package com.example.pukaar_therapist.Fragments

import Adapters.Session_summary_tab_Adapter
import Session_Summary_Fragment.All_Session_Fragment
import Session_Summary_Fragment.Pending_Session_Fragment
import Session_Summary_Fragment.one_to_one_Fragment
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.pukaar_therapist.R
import com.example.pukaar_therapist.databinding.FragmentSessionSummaryBinding
import com.google.android.material.tabs.TabLayout

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Session_summary_Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Session_summary_Fragment : Fragment() {
lateinit var sessionSummaryBinding: FragmentSessionSummaryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
     sessionSummaryBinding = FragmentSessionSummaryBinding.inflate(inflater , container , false)
        val textView = requireActivity().findViewById<TextView>(R.id.title_toolbar)
        textView.setText("Session Summary")

        var viewpager : ViewPager = sessionSummaryBinding.sesionSummaryViewpager
        var tabLayout : TabLayout = sessionSummaryBinding.dwm1

       val sessionSummaryTabAdapter = Session_summary_tab_Adapter(childFragmentManager)
        sessionSummaryTabAdapter.addfragments(All_Session_Fragment() , "All Sessions")
        sessionSummaryTabAdapter.addfragments(Pending_Session_Fragment() , "Pending Sessions")
        sessionSummaryTabAdapter.addfragments(one_to_one_Fragment() , "1 On 1 Sessions")
        viewpager.adapter = sessionSummaryTabAdapter
        tabLayout.setupWithViewPager(viewpager)

        return sessionSummaryBinding.root
    }



}