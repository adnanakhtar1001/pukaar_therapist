package com.example.pukaar_therapist.Fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.pukaar_therapist.APIClient.APIClient
import com.example.pukaar_therapist.API_Interface.API_Interfaces
import com.example.pukaar_therapist.CommonFunction
import com.example.pukaar_therapist.Navigation
import com.example.pukaar_therapist.R
import com.example.pukaar_therapist.Responces.ClientDiaryResponce
import com.example.pukaar_therapist.Responces.LoginResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Set_mood_Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Set_mood_Fragment : Fragment() {

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var apiInterfaces: API_Interfaces

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       val view = inflater.inflate(R.layout.fragment_set_mood_, container, false)
        val textView = view.findViewById<TextView>(R.id.title_toolbar)
        val feelingEditText = view.findViewById<EditText>(R.id.feeling_et)
        val seekbar1 = view.findViewById<com.warkiz.widget.IndicatorSeekBar>(R.id.seekbar1)
        val seekbar2 = view.findViewById<com.warkiz.widget.IndicatorSeekBar>(R.id.seekbar2)
        val seekbar3 = view.findViewById<com.warkiz.widget.IndicatorSeekBar>(R.id.seekbar3)
        val save = view.findViewById<Button>(R.id.set_mode1_button1)
        apiInterfaces = APIClient.create()
        var bundle = arguments
        val id = bundle?.getString("id")
        save.setOnClickListener(View.OnClickListener {
            if(feelingEditText.text.trim().toString().isNotEmpty()) {
                val setClient = apiInterfaces.setClientDiary(CommonFunction.getToken(requireContext()),"sad",seekbar1.progress.toString(),seekbar2.progress.toString(),seekbar3.progress.toString(),feelingEditText.text.toString(),id.toString())
                setClient.enqueue(object : Callback<String> {
                    override fun onResponse(
                        call: Call<String>?,
                        response: Response<String>?
                    ) {

                        if (response?.body() != null) {
                            if(response.body().equals("Success"))
                                Toast.makeText(requireContext(),"Success",Toast.LENGTH_SHORT).show()
                            else
                                Toast.makeText(requireContext(),"Error",Toast.LENGTH_SHORT).show()
                        }

                    }

                    override fun onFailure(call: Call<String>?, t: Throwable?) {

                    }
                })
            }
            else
                Toast.makeText(requireContext(),"Please Enter required Fields...",Toast.LENGTH_SHORT).show()
        })

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Set_mood_Fragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Set_mood_Fragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}