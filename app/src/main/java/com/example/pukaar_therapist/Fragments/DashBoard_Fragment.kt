package com.example.pukaar_therapist.Fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.pukaar_therapist.CommonFunction
import com.example.pukaar_therapist.Navigation
import com.example.pukaar_therapist.R
import com.example.pukaar_therapist.UnAssigned_user_Fragment
import com.example.pukaar_therapist.databinding.FragmentDashBoardBinding


class DashBoard_Fragment : Fragment() {

    lateinit var dashBoardBinding: FragmentDashBoardBinding
    lateinit var assignedUserFragment: Assigned_User_Fragment
    lateinit var unassignedUserFragment: UnAssigned_user_Fragment
    lateinit var sessionSummaryFragment: Session_summary_Fragment
    lateinit var dashboardFragment: DashBoard_Fragment

lateinit var navigation: Navigation


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        getActivity()?.setTitle("")


        dashBoardBinding = FragmentDashBoardBinding.inflate(inflater, container, false)
        dashBoardBinding.therapistName.text = CommonFunction.getName(requireContext())

        val textView = requireActivity().findViewById<TextView>(R.id.title_toolbar)
        textView.setText("Pukaar")
        dashBoardBinding.cardView.setOnClickListener{

           assignedUserFragment = Assigned_User_Fragment()
            fragmentManager?.beginTransaction()?.replace(R.id.container ,assignedUserFragment)?.commit()
        }

dashBoardBinding.cardView4.setOnClickListener{
    unassignedUserFragment = UnAssigned_user_Fragment()
    fragmentManager?.beginTransaction()?.replace(R.id.container , unassignedUserFragment)?.commit()
}
        dashBoardBinding.cardView5.setOnClickListener{
            sessionSummaryFragment = Session_summary_Fragment()
            fragmentManager?.beginTransaction()?.replace(R.id.container , sessionSummaryFragment)?.commit()

        }
        dashBoardBinding.cardView6.setOnClickListener{

            fragmentManager?.beginTransaction()?.replace(R.id.container , Assigned_User_Fragment())?.commit()

        }


        return dashBoardBinding.root
    }


}