package com.example.pukaar_therapist.Fragments

import Adapters.Assigned_Recyceler_Adapter
import Adapters.DayFragment_Recyceler_Adapter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pukaar_therapist.APIClient.APIClient
import com.example.pukaar_therapist.API_Interface.API_Interfaces
import com.example.pukaar_therapist.CommonFunction
import com.example.pukaar_therapist.R
import com.example.pukaar_therapist.Responces.ClientDiaryResponce
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Day_Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Day_Fragment : Fragment() {
    private lateinit var apiInterfaces: API_Interfaces
    private lateinit var ids: String

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_day_, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        apiInterfaces = APIClient.create()
        var bundle = arguments
        val ids = bundle?.getString("id")
        val getClient = apiInterfaces.getClientDiary(CommonFunction.getToken(requireContext()),ids.toString())
        getClient.enqueue(object : Callback<ClientDiaryResponce> {
            override fun onResponse(
                call: Call<ClientDiaryResponce>?,
                response: Response<ClientDiaryResponce>?
            ) {
                if (response?.body() != null) {
                    recyclerView.adapter = DayFragment_Recyceler_Adapter(requireContext(), response.body()!!.data.data[0].therapist_client_diary)
                    recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                }

            }

            override fun onFailure(call: Call<ClientDiaryResponce>?, t: Throwable?) {

            }
        })
        return view
    }

    fun setId(id: String) {
        this.ids = id
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Day_Fragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Day_Fragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}