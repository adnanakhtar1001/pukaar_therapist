package com.example.pukaar_therapist.Fragments

import Adapters.Session_summary_tab_Adapter
import android.os.Bundle
import android.renderscript.ScriptGroup
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.example.pukaar_therapist.R
import com.google.android.material.tabs.TabLayout


class Daily_Dairy_Fragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_daily__dairy_, container, false)
        var bundle = arguments
        val id = bundle?.getString("id")
        val textView = view.findViewById<TextView>(R.id.title_toolbar)
        //textView.setText("Daily Dairy")


        val viewpager  = view.findViewById<ViewPager>(R.id.session_summary_viewpager)
        val tabLayout = view.findViewById<TabLayout>(R.id.dwm3)


        //intilize Adapter
        val sessionSummaryTabAdapter = Session_summary_tab_Adapter(childFragmentManager)
        val fragment = Day_Fragment()
        fragment.setId(id.toString())
        fragment.arguments = bundle

        val fragment1 = Day_Fragment()
        fragment1.setId(id.toString())
        fragment1.arguments = bundle

        val fragment2 = Day_Fragment()
        fragment2.setId(id.toString())
        fragment2.arguments = bundle

        sessionSummaryTabAdapter.addfragments(fragment , "Day")
        sessionSummaryTabAdapter.addfragments(fragment1, "Week")
        sessionSummaryTabAdapter.addfragments(fragment2 , "Month")

        viewpager.adapter = sessionSummaryTabAdapter
        tabLayout.setupWithViewPager(viewpager)

        return view
    }


}