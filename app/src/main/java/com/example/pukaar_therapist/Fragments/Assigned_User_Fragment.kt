package com.example.pukaar_therapist.Fragments

import Adapters.Assigned_Recyceler_Adapter
import Adapters.UnAssigned_Recyceler_Adapter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pukaar_therapist.APIClient.APIClient
import com.example.pukaar_therapist.API_Interface.API_Interfaces
import com.example.pukaar_therapist.CommonFunction
import com.example.pukaar_therapist.R
import com.example.pukaar_therapist.Responces.User_List_Responce
import com.example.pukaar_therapist.databinding.FragmentAssignedUserBinding
import com.example.pukaar_therapist.databinding.FragmentUnAssignedUserBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Assigned_User_Fragment : Fragment() {

    lateinit var assignedUserBinding: FragmentAssignedUserBinding
    lateinit var apiInterfaces: API_Interfaces
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val textView = requireActivity().findViewById<TextView>(R.id.title_toolbar)
        textView.setText("Assigned User")
        assignedUserBinding = FragmentAssignedUserBinding.inflate(inflater , container , false)
        apiInterfaces = APIClient.create()
        val unassignedUser = apiInterfaces.getUserTherapistResponse(CommonFunction.getToken(requireContext()),"assigned")
        unassignedUser.enqueue( object : Callback<User_List_Responce> {
            override fun onResponse(call: Call<User_List_Responce>?, response: Response<User_List_Responce>?) {

                if(response?.body() != null)
                {
                    if( response.body()!!.users.data != null) {
                        val recyclerView = assignedUserBinding.assignedUserRecycler
                        recyclerView.adapter =
                            context?.let { Assigned_Recyceler_Adapter(it, response.body()!!.users.data) }
                        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    }
                }
            }

            override fun onFailure(call: Call<User_List_Responce>?, t: Throwable?) {
                Toast.makeText(requireContext(),"Error...",
                    Toast.LENGTH_LONG).show();
            }
        })
        return assignedUserBinding.root
    }


}