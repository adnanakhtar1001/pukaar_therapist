package com.example.pukaar_therapist

import Adapters.UnAssigned_Recyceler_Adapter
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pukaar_therapist.APIClient.APIClient
import com.example.pukaar_therapist.API_Interface.API_Interfaces

import com.example.pukaar_therapist.Responces.TherapistListResponse1

import com.example.pukaar_therapist.databinding.FragmentUnAssignedUserBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class UnAssigned_user_Fragment : Fragment() {

    lateinit var unAssignedUserBinding: FragmentUnAssignedUserBinding
    private lateinit var apiInterface: API_Interfaces
    private lateinit var mContext: Context
   /* val profile_image : List<Int> = listOf( R.drawable.profile_image , R.drawable.profile_image)
    val profile_name : List<String> = listOf("Uzair Afzal" , "Maliha Ashfeen")
    val time : List<String> = listOf("09:10 AM" , "09:10 AM")*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        unAssignedUserBinding = FragmentUnAssignedUserBinding.inflate(inflater , container , false)
        val textView = requireActivity().findViewById<TextView>(R.id.title_toolbar)
        textView.setText("UnAssigned User")



        apiInterface = APIClient.create()
        val unassignedUser = apiInterface.getUserTherapistResponse1(CommonFunction.getToken(requireContext()),"client","active","unassigned")
        unassignedUser.enqueue( object : Callback<TherapistListResponse1> {
            override fun onResponse(call: Call<TherapistListResponse1>?, response: Response<TherapistListResponse1>?) {

                if(response?.body() != null)
                {
                    if( response.body()!!.users.data != null) {
                        val recyclerView = unAssignedUserBinding.unassignedUserRecycler
                        recyclerView.adapter =
                            context?.let { UnAssigned_Recyceler_Adapter(it, response.body()!!.users.data) }
                        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    }
                }
            }

            override fun onFailure(call: Call<TherapistListResponse1>?, t: Throwable?) {
                Toast.makeText(requireContext(),"Error...",
                    Toast.LENGTH_LONG).show();
            }
        })


/*
val recyclerView = unAssignedUserBinding.unassignedUserRecycler
        recyclerView.adapter = UnAssigned_Recyceler_Adapter(profile_image , profile_name , time)
        recyclerView.layoutManager = LinearLayoutManager(context , LinearLayoutManager.VERTICAL , false)
*/


        return unAssignedUserBinding.root
    }


}