package com.example.pukaar_therapist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.pukaar_therapist.APIClient.APIClient
import com.example.pukaar_therapist.API_Interface.API_Interfaces
import com.example.pukaar_therapist.Responces.LoginResponse
import com.example.pukaar_therapist.databinding.ActivityLoginBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login : AppCompatActivity() {

    private lateinit var loginBinding: ActivityLoginBinding
    lateinit var apiInterfaces: API_Interfaces

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginBinding = ActivityLoginBinding.inflate(layoutInflater)

        setContentView(loginBinding.root)

        apiInterfaces = APIClient.create()
        loginBinding.loginScreenButton.setOnClickListener {


                val loginResponse = apiInterfaces.getLoginResponse(
                    loginBinding.mainEmail.text.toString(),
                    loginBinding.editText.text.toString()
                )
                loginResponse.enqueue(object : Callback<LoginResponse> {
                    override fun onResponse(
                        call: Call<LoginResponse>?,
                        response: Response<LoginResponse>?
                    ) {

                        if (response?.body() != null) {
                            CommonFunction.saveToken(applicationContext, response.body()!!.data.token)
                            CommonFunction.saveName(applicationContext, response.body()!!.data.first_name + " " + response.body()!!.data.last_name)
                            CommonFunction.saveId(applicationContext, response.body()!!.data.user_id )

                            overridePendingTransition(0, 0)

                            var isTherapist: Boolean = false
                            for (item: String in response.body()!!.data.role) {
                                if (item == "therapist")
                                    isTherapist = true
                            }
                            if (isTherapist) {
                                CommonFunction.saveToken(applicationContext, response.body()!!.data.token)
                                CommonFunction.saveName(applicationContext, response.body()!!.data.first_name + " " + response.body()!!.data.last_name
                                )
                                val intent = Intent(this@Login, Navigation::class.java)
                                startActivity(intent)
                                overridePendingTransition(0, 0)
                            } else
                                Toast.makeText(
                                    applicationContext,
                                    "Please Enter Therapist Email and Password...",
                                    Toast.LENGTH_LONG
                                ).show();

                        } else
                            Toast.makeText(
                                applicationContext,
                                "Invalid Email and Password...",
                                Toast.LENGTH_LONG
                            ).show();
                    }

                    override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {

                    }
                })

            }
        }
    }


