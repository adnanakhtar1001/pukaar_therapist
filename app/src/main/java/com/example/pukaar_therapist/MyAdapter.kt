package com.example.pukaar_therapist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyAdapter(val allsessions: MutableList<all_sessions_data_class>) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater : LayoutInflater = LayoutInflater.from(parent.context)
        val view : View = inflater.inflate(R.layout.all_session_history , parent ,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      holder.patinet_name.text[position]
    }

    override fun getItemCount(): Int {
        return allsessions.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        var patinet_name = itemView.findViewById<TextView>(R.id.patient_name)
        var date = itemView.findViewById<TextView>(R.id.date)
        var time = itemView.findViewById<TextView>(R.id.time)
        var session_taken = itemView.findViewById<TextView>(R.id.session_taken)

    }
}

