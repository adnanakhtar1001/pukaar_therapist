package com.example.pukaar_therapist.API_Interface


import android.content.Context
import com.example.pukaar_therapist.Responces.*
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST

interface API_Interfaces {

    @FormUrlEncoded
    @POST("login")
    fun getLoginResponse(@Field("email") email: String, @Field("password") password: String) : Call<LoginResponse>

    @FormUrlEncoded
    @POST("user")
    fun getUserTherapistResponse(@Header("Authorization") header: String, @Field("type") type: String) : Call<User_List_Responce>
    @FormUrlEncoded
    @POST("user")
    fun getUserTherapistResponse1(@Header("Authorization") header: String,@Field("role") role: String,@Field("status") status: String,@Field("type") type: String) : Call<TherapistListResponse1>

    @FormUrlEncoded
    @POST("diary/therapist/filters")
    fun getClientDiary(@Header("Authorization") header: String, @Field("client_id") clientId: String) : Call<ClientDiaryResponce>

    @FormUrlEncoded
    @POST("diary/therapist/store")
    fun setClientDiary(@Header("Authorization") header: String, @Field("mood") mood: String, @Field("anxiety") anxiety: String, @Field("energy") energy: String, @Field("self_confidence") self_confidence: String,@Field("feeling") feeling: String,@Field("client_id") client_id: String) : Call<String>

    @FormUrlEncoded
    @POST("accept/therapist")
    fun getUserSessionUpdate(@Header("Authorization") header: String,@Field("client_id") client_id: Int ,@Field("therapist_id") therapist_id: String) : Call<String>
}
