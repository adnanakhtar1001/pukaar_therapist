package com.example.pukaar_therapist

import android.content.Context
import android.content.SharedPreferences
import java.text.SimpleDateFormat

class CommonFunction {

    companion object {
        fun saveToken(context: Context, token: String) {
            val sharedPreferences: SharedPreferences =
                context.getSharedPreferences("PukaarTherapist", Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.putString("token", token)
            editor.apply()
            editor.commit()
        }

        fun getToken(context: Context): String {
            val sharedPreferences: SharedPreferences =
                context.getSharedPreferences("PukaarTherapist", Context.MODE_PRIVATE)
            val token = sharedPreferences.getString("token", "")
            return "Bearer " + token.toString();
        }

        fun saveId(context: Context, id: String) {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences("PukaarTherapist1", Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.putString("therapist_id", id)
            editor.apply()
            editor.commit()
        }
        fun get_id(context: Context): String {
            val sharedPreferences: SharedPreferences = context.getSharedPreferences("PukaarTherapist1", Context.MODE_PRIVATE)
            val id = sharedPreferences.getString("therapist_id", "0")
            return id.toString();
        }

        fun saveName(context: Context, name: String) {
            val sharedPreferences: SharedPreferences =
                context.getSharedPreferences("PukaarAdmin", Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.putString("name", name)
            editor.apply()
            editor.commit()
        }

        fun getName(context: Context): String {
            val sharedPreferences: SharedPreferences =
                context.getSharedPreferences("PukaarAdmin", Context.MODE_PRIVATE)
            val token = sharedPreferences.getString("name", "")
            return token.toString();
        }
        fun dateFormat(dateTime: String): String {
            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val formatter = SimpleDateFormat("dd-MM-yyyy hh:mm a")
            return formatter.format(parser.parse(dateTime))
        }
    }
}