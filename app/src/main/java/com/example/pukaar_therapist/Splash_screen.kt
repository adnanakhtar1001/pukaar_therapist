package com.example.pukaar_therapist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class Splash_screen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            if(CommonFunction.getToken(applicationContext).length>10)
            {
                val intent = Intent(this, Navigation::class.java);
                startActivity(intent);
                finish();
            }
            else {
                val intent = Intent(this, Login::class.java);
                startActivity(intent);
                finish();
            }
        }, 2000)
    }
}