package com.example.pukaar_therapist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.pukaar_therapist.databinding.ActivityOnboardingScreenBinding

class Onboarding_screen : AppCompatActivity() {

    lateinit var onboardingScreenBinding: ActivityOnboardingScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onboardingScreenBinding = ActivityOnboardingScreenBinding.inflate(layoutInflater)
        setContentView(onboardingScreenBinding.root)

        onboardingScreenBinding.onboardingButton.setOnClickListener{

            val intent = Intent(this , Login::class.java)
            startActivity(intent)

        }

    }
}