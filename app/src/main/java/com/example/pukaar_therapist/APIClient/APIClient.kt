package com.example.pukaar_therapist.APIClient

import com.example.pukaar_therapist.API_Interface.API_Interfaces
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class APIClient {
    companion object {

        var BASE_URL = "https://pukar.qareeb.com/api/"

        fun create() : API_Interfaces {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(API_Interfaces::class.java)

        }
    }
}


/*
fun create() : ApiInterface {

    val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()
    return retrofit.create(ApiInterface::class.java)

}*/
