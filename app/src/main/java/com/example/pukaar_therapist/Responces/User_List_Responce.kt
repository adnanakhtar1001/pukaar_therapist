package com.example.pukaar_therapist.Responces

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User_List_Responce (var users : Users) : Parcelable

@Parcelize
data class Users(var current_page: String, var data: ArrayList<UsersData>, var to: Int, var total: Int): Parcelable
@Parcelize
data class UsersData(var id: Int, var first_name: String, var last_name: String, var mobile_number: String, var email : String,
                     var user_status: UserStatus, var created_at: String,var updated_at: String , var client_profile: ClientProfile): Parcelable
@Parcelize
data class UserStatus(var id: Int, var name: String): Parcelable
@Parcelize
data class ClientProfile(var id: Int , var  orientation : String , var religion : String , var religion_identifier : String , var medicines : String , var sleeping_habit : String , var problem : String) : Parcelable


