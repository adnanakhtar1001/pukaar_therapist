package com.example.pukaar_therapist.Responces

import android.provider.ContactsContract

data class ClientDiaryResponce(var data : DataClient)
data class DataClient(var data: ArrayList<DataDiary>)
data class DataDiary(var therapist_client_diary: ArrayList<DataDiary1>)
data class DataDiary1(var id: Int, var mood: String, var anxiety: String, var energy: String, var self_confidence : String, var feeling: String,
var therapist_profile_id: String,var created_at: String,var updated_at: String)